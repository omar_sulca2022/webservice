package es.paradores.demo.repositories;

import es.paradores.demo.models.CromoModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CromoRepository extends CrudRepository<CromoModel, Integer> {
}
