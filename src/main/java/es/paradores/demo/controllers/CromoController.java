package es.paradores.demo.controllers;

import es.paradores.demo.models.CromoModel;
import es.paradores.demo.services.CromoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/cromo")
public class CromoController {
    @Autowired
    CromoService cromoService;

    @GetMapping()
    public ArrayList<CromoModel> getCromos(){
        return cromoService.getCromos();
    }

    @PostMapping()
    public CromoModel saveCromo(@RequestBody CromoModel cromo){
        return cromoService.saveCromo(cromo);
    }
}
