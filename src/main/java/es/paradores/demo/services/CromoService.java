package es.paradores.demo.services;

import es.paradores.demo.models.CromoModel;
import es.paradores.demo.repositories.CromoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CromoService {
    @Autowired
    CromoRepository cromoRepository;

    public ArrayList<CromoModel> getCromos(){
        return (ArrayList<CromoModel>) cromoRepository.findAll();
    }

    public CromoModel saveCromo(CromoModel cromo){
        return cromoRepository.save(cromo);
    }
}
