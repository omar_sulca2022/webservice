package es.paradores.demo.models;

import javax.persistence.*;

@Entity
@Table(name = "cromos")
public class CromoModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private int idcromos;

    private int numero;

    public int getIdcromos() {
        return idcromos;
    }

    public void setIdcromos(int idcromos) {
        this.idcromos = idcromos;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
}
