package es.paradores.demo.models;

import javax.persistence.*;

@Entity
@Table(name = "tip_tipotest")
public class TipoTestModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private int id;

    private String nombre;
    private String codigo;
    private String conf;
    private int laboratorio;
    private int eliminado;
    private int datosbasales;
    private int datosbasales_seg;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getConf() {
        return conf;
    }

    public void setConf(String conf) {
        this.conf = conf;
    }

    public Integer getLaboratorio() {
        return laboratorio;
    }

    public void setLaboratorio(Integer laboratorio) {
        this.laboratorio = laboratorio;
    }

    public Integer getEliminado() {
        return eliminado;
    }

    public void setEliminado(Integer eliminado) {
        this.eliminado = eliminado;
    }

    public Integer getDatosbasales() {
        return datosbasales;
    }

    public void setDatosbasales(Integer datosbasales) {
        this.datosbasales = datosbasales;
    }

    public Integer getDatosbasales_seg() {
        return datosbasales_seg;
    }

    public void setDatosbasales_seg(Integer datosbasales_seg) {
        this.datosbasales_seg = datosbasales_seg;
    }
}
